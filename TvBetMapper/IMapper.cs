﻿namespace TvBetMapper;

public interface IMapper<TTarget, TSource>
{
    TTarget Map(TSource source);
    IEnumerable<TTarget> Map(IEnumerable<TSource> source);
}