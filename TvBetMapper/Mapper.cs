﻿namespace TvBetMapper;

public class Mapper<TTarget, TSource> :IMapper<TTarget, TSource>
{
    private readonly Func<TSource, TTarget> _convertFunc;

    public Mapper(Func<TSource, TTarget> convertFunc)
    {
        _convertFunc = convertFunc;
    }
    
    public TTarget Map(TSource source)
    {
        return _convertFunc(source);
    }

    public IEnumerable<TTarget> Map(IEnumerable<TSource> source)
    {
        return source
            .Select(this.Map);
    }
}