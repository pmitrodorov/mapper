﻿using Microsoft.Extensions.DependencyInjection;
using TvBetMapper.Model;

namespace TvBetMapper.Extensions.DependencyInjection;

public static class TvBetCustomExtensionMethods
{
    public static IServiceCollection AddMapConfiguration<TTarget, TSource>(this IServiceCollection services, IMapperConfiguration<TTarget, TSource> configuration)
    {
        services.AddSingleton<IMapper<TTarget, TSource>>(configuration.CreateMappper());
        return services;
    }
    
    public static IServiceCollection AddMapProfile(this IServiceCollection services, Action<MapProfileOption> option)
    {
        var opt = new MapProfileOption();
        option.Invoke(opt);
        services.AddSingleton<IMapProfile>(new MapProfile(opt.GetMapProfile()));
        return services;
    }
}