﻿namespace TvBetMapper;

public interface IMapProfile
{
    TTarget Map<TTarget, TSource>(TSource source);
}