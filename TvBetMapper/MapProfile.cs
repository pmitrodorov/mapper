﻿namespace TvBetMapper;

public class MapProfile: IMapProfile
{
    private readonly Dictionary<(Type target , Type source), object> _mapProfiles;

    public MapProfile(Dictionary<(Type target , Type source), object> mapProfiles)
    {
        _mapProfiles = mapProfiles;
    }

    public TTarget Map<TTarget, TSource>(TSource source)
    {
        (Type target, Type source) key = (typeof(TTarget),typeof(TSource));
        _mapProfiles.TryGetValue(key,  out var value);
        if (value is IMapper<TTarget, TSource> mapper)
        {
            return mapper.Map(source);
        }
        else
        {
            throw new ArgumentNullException($"Not found IMapConfiguration<{typeof(TTarget).Name},{typeof(TSource).Name}>");
        }
    }
    
}