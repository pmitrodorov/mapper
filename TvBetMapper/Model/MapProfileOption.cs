﻿namespace TvBetMapper.Model;

public class MapProfileOption
{

    private readonly Dictionary<(Type target , Type source), object> _mapProfiles = new Dictionary<(Type target, Type source), object>();
    
    public void AddProfile<TTarget, TSource>(IMapperConfiguration<TTarget, TSource> configuration)
    {
        (Type target, Type source) key = (typeof(TTarget),typeof(TSource));
        _mapProfiles.Add(key, configuration.CreateMappper());
    }

    public Dictionary<(Type target, Type source), object> GetMapProfile()
    {
        return _mapProfiles;
    }
    
}