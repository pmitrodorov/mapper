﻿namespace TvBetMapper;

public interface IMapperConfiguration<TTarget, TSource>
{
    public IMapper<TTarget, TSource> CreateMappper();
}