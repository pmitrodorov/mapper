﻿namespace TvBetMapper;

public abstract class MapperConfiguration<TTarget, TSource>: IMapperConfiguration<TTarget, TSource>
{
    protected abstract TTarget Configure(TSource source);
    
    public IMapper<TTarget, TSource> CreateMappper()
    {
        return new Mapper<TTarget, TSource>(Configure);
    }
}