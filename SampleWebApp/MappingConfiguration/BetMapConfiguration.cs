﻿using SampleWebApp.Domain;
using SampleWebApp.DTO;
using TvBetMapper;

namespace SampleWebApp.MappingConfiguration;

public class BetMapConfiguration: MapperConfiguration<BetDTO, Bet>
{
    protected override BetDTO Configure(Bet source)
    {
        return new BetDTO()
        {
            Id = source.Id,
            Events = source.Events.Select(e => new EventMapConfiguration().CreateMappper().Map(e)).ToList()
        };
    }
}