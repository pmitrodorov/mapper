﻿using SampleWebApp.Domain;
using SampleWebApp.DTO;
using TvBetMapper;

namespace SampleWebApp.MappingConfiguration;

public class EventMapConfiguration: MapperConfiguration<EventDTO, Event>
{
    protected override EventDTO Configure(Event source)
    {
        return new EventDTO()
        {
            code = source.code,
            Type = source.Type
        };
    }
}