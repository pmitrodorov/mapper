﻿namespace SampleWebApp.DTO;

public class BetDTO
{
    public int Id { get; set; }
    public List<EventDTO> Events { get; set; }
}