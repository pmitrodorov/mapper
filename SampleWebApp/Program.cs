using SampleWebApp;
using SampleWebApp.Domain;
using SampleWebApp.DTO;
using SampleWebApp.MappingConfiguration;
using TvBetMapper;
using TvBetMapper.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddSingleton<Repository>();
builder.Services.AddMapConfiguration(new BetMapConfiguration());
builder.Services.AddMapProfile(o =>
{
    o.AddProfile(new BetMapConfiguration());
    o.AddProfile(new EventMapConfiguration());
});
builder.Services.AddSingleton<IMapper<EventDTO, Event>>(new Mapper<EventDTO, Event>(x =>
{
    return new EventDTO()
    {
        code = x.code,
        Type = x.Type
    };
})); 

var app = builder.Build();

app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapDefaultControllerRoute();
    endpoints.MapControllers();
});

app.Run();