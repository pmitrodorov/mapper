﻿using Microsoft.AspNetCore.Mvc;
using SampleWebApp.Domain;
using SampleWebApp.DTO;
using TvBetMapper;

namespace SampleWebApp.Controllers;

[ApiController]
public class BetController: ControllerBase
{
    private readonly Repository _rep;
    private readonly IMapper<BetDTO, Bet> _betMapper;
    private readonly IMapper<EventDTO, Event> _eventMapper;
    private readonly IMapProfile _mapProfile;
    public BetController(Repository rep, IMapper<BetDTO, Bet> betMapper, IMapper<EventDTO, Event> eventMapper, IMapProfile mapProfile)
    {
        _rep = rep;
        _betMapper = betMapper;
        _eventMapper = eventMapper;
        _mapProfile = mapProfile;
    }
    [HttpGet]
    [Route("bet")]
    public IActionResult GetBetItem()
    {
        var result = _betMapper.Map(_rep.GetBet());
        return Ok(result);
    }
    
    [HttpGet]
    [Route("bets")]
    public IActionResult GetBetsItem()
    {
        var result = _betMapper.Map(_rep.GetBets());
        return Ok(result);
    }
    
    [HttpGet]
    [Route("event")]
    public IActionResult GetEvetItem()
    {
        var result = _mapProfile.Map<EventDTO, Event>(_rep.GetEvent());
        return Ok(result);
    }
    
    [HttpGet]
    [Route("events")]
    public IActionResult GetEvetsItem()
    {
        var result = _eventMapper.Map(_rep.GetEvents());
        return Ok(result);
    }
}