﻿using SampleWebApp.Domain;

namespace SampleWebApp;

public class Repository
{
    public List<Bet> GetBets()
    {
        var event1 = new Event() {code = "1", Type = "e"};
        var event2 = new Event() {code = "2", Type = "f"};
        var events = new List<Event>()
        {
            event1, event2
        };
        var bet = new Bet()
        {
            Id = 123,
            Events = events
        };
        var bet2 = new Bet()
        {
            Id = 127,
            Events = events
        };
        return new List<Bet>()
        {
            bet, bet2
        };
    }
    
    public Bet GetBet()
    {
        var event1 = new Event() {code = "1", Type = "e"};
        var event2 = new Event() {code = "2", Type = "f"};
        var events = new List<Event>()
        {
            event1, event2
        };
        return new Bet()
        {
            Id = 123,
            Events = events
        };
    }

    public Event GetEvent()
    {
        return new Event() {code = "1", Type = "e"};
    }
    
    public List<Event> GetEvents()
    {
        var event1 = new Event() {code = "1", Type = "e"};
        var event2 = new Event() {code = "2", Type = "f"};
        return new List<Event>()
        {
            event1, event2
        };
    }
}