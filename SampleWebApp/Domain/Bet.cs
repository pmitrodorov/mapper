﻿namespace SampleWebApp.Domain;

public class Bet
{
    public int Id { get; set; }
    public List<Event> Events { get; set; }
}