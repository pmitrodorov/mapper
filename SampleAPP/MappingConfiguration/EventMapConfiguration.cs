﻿using SampleAPP.Domain;
using SampleAPP.DTO;
using TvBetMapper;

namespace SampleAPP.MappingConfiguration;

public class EventMapConfiguration: MapperConfiguration<EventDTO, Event>
{
    protected override EventDTO Configure(Event source)
    {
        return new EventDTO()
        {
            code = source.code,
            Type = source.Type
        };
    }
}