﻿using SampleAPP.Domain;
using SampleAPP.DTO;
using TvBetMapper;

namespace SampleAPP.MappingConfiguration;

public class BetMapConfiguration: MapperConfiguration<BetDTO, Bet>
{
    protected override BetDTO Configure(Bet source)
    {
        return new BetDTO()
        {
            Id = source.Id,
            Events = source.Events.Select(e => new EventMapConfiguration().CreateMappper().Map(e)).ToList()
        };
    }
}