﻿using SampleAPP.Domain;
using SampleAPP.DTO;
using SampleAPP.MappingConfiguration;
using TvBetMapper;

var event1 = new Event() {code = "1", Type = "e"};
var event2 = new Event() {code = "2", Type = "f"};
var events = new List<Event>()
{
event1, event2
};
var bet = new Bet()
{
    Id = 123,
    Events = events
};
var bet2 = new Bet()
{
    Id = 127,
    Events = events
};


var Betmapper = new BetMapConfiguration().CreateMappper();
var betDTO1 = Betmapper.Map(bet);
var betDTO2 = Betmapper.Map(bet2);
var eventMapper = new Mapper<EventDTO, Event>(s =>
{
    return new EventDTO()
    {
        code = s.code,
        Type = s.Type
    };
});


var eventDTO1 = eventMapper.Map(event1);


Console.WriteLine("Mapping!");
Console.ReadLine();